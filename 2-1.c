#include <stdio.h>

int get_num(){
char c = getchar();
int a = 0;
int neg = 0;

if(c == '-') {
   neg = 1;
   c = getchar();
}

while(isdigit(c)) {
    a = a*10 + (c - '0');
    c = getchar();
}

if(neg == 1) {
    a = a * -1;
}
return a;
}

int smallest(int number){
    int i,d,result=0;
    int freq[10]={0};//initialize frequency of each digit to Zero//
    while(number>0){
        d = number % 10;// extract last digit
        freq[d]++;// increment counting
        number = number / 10;//remove last digit
    }
        // arrange all remaining digits
    for(i=0;i<10;i++){
        while(freq[i]>0){
            result=result*10+i;
            freq[i] = freq[i] - 1;
        }

    }
    return result;
}

int main(){
    int a,number;
    a = get_num();
    number = smallest(a);
    printf("%d",number);
    return 0;
}

